import React from 'react';
import classNames from 'classnames';
import './App.css';
import useKeyPress from './use-key-press';

const EMPTY = [null, null, null, null, null, null, null, null, null, null, null, null, null]

const MAP = [
  EMPTY,
  [null, 1, null, 1, null, 1, null, 1, null, 1, null, 1, null],
  [null, 1, null, 1, null, 1, null, 1, null, 1, null, 1, null],
  [null, 1, null, 1, null, 1, null, 1, null, 1, null, 1, null],
  EMPTY,
  [2, null, 1, 1, null, null, null, null, null, 1, 1, null, null],
  [null, null, null, null, null, 1, null, 1, null, null, null, null, null],
  [null, 1, null, 1, null, 1, null, 1, null, 1, null, 1, null],
  [null, 1, null, 1, null, null, null, null, null, 1, null, 1, null],
  [null, null, null, null, null, 1, 1, 1, null, null, null, null, null]
];

function App() {
  const upPress = useKeyPress('w');
  const leftPress = useKeyPress('a');
  const downPress = useKeyPress('s');
  const rightPress = useKeyPress('d');

  const [start, setStart] = React.useState(false);
  const [spritePos, setSprPos] = React.useState({ x: 0, y: 0 });
  const [arr, setArr] = React.useState(null);



  React.useEffect(() => {
    if (upPress || leftPress || downPress || rightPress) {
      setStart(true);
    }
    if (upPress) {
      setArr('w');
    }
    if (leftPress) {
      setArr('a');
    }
    if (downPress) {
      setArr('s');
    }
    if (rightPress) {
      setArr('d');
    }
  }, [setArr, upPress, leftPress, downPress, rightPress]);

  React.useEffect(() => {
    let to;
    clearInterval(to);
    switch (arr) {
      case 'w': {
        clearTimeout(to);
        to = setTimeout(() => {
          setSprPos(({ x, y }) => {
            if (y <= 0) {
              clearTimeout(to);
              return ({x, y});
            }
            return ({x, y: y - 7});
          });
          console.log(spritePos);
        }, 100);
        break;
      }
      case 'a': {
        clearTimeout(to);
        to = setTimeout(() => {
          setSprPos(({ x, y }) => {
            if (x <= 0) {
              clearTimeout(to);
              return ({x, y});
            }
            return ({x: x - 7, y});
          });
          console.log(spritePos);
        }, 100);
        break;
      }
      case 'd': {
        clearTimeout(to);
        to = setTimeout(() => {
          setSprPos(({ x, y }) => ({ x: x + 7, y }));
          console.log(spritePos);
        }, 100);
        break;
      }
      case 's': {
        clearTimeout(to);
        to = setTimeout(() => {
          setSprPos(({ x, y }) => ({ x, y: y + 7 }));
          console.log(spritePos);
        }, 100);
        break;
      }
    }
  }, [arr, spritePos]);


  const renderMap = MAP.map(line => {
    return <tr>{line.map(pole => {
      return <td  className={classNames(['pole', {brick: pole === 1, wall: pole === 2}])}/>
    })}</tr>
  })

  return (
    <>
      {start ? <strong>Игра началась</strong> : <strong>Нажми w a s или d</strong>}
    <div className="App">

      <div className={classNames('sprite', 'my')} style={{ top: spritePos.y, left: spritePos.x }}/>
      <table className="map">
        {renderMap}
      </table>
    </div>
      </>
  );
}

export default App;
